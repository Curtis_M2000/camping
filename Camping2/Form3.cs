﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Camping2
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        public double Tax(double prixtax)
        {
            double prix;
            prix = prixtax + (0.15 * prixtax);
            return prix;
        }

        private static double prixtot;

        public double PRIXTOT
        {
            get { return prixtot; }
            set { prixtot = value; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form3 frm3 = new Form3();
            frm3.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int nbperso = Convert.ToInt32(numericUpDown1.Value);
            int nbparcour = Convert.ToInt32(numericUpDown2.Value);
            double prix=0, prixtax;

            if(radioButton1.Checked == true)
            {
                if (nbparcour == 1)
                {
                    prix = 18.25;
                }
                else if (nbparcour == 2)
                {
                    prix = 25.00;
                }
                else if (nbparcour == 3)
                {
                    prix = 27.75;
                }
            }
            else if (radioButton2.Checked == true)
            {
                if (nbparcour == 1)
                {
                    prix = 15.25;
                }
                else if (nbparcour == 2)
                {
                    prix = 22.75;
                }
                else if (nbparcour == 3)
                {
                    prix = 25.25;
                }
            }

            PRIXTOT = prix * nbperso;
            prixtax = Tax(PRIXTOT);

            label6.Text = "Nombre de personne = " + nbperso + ". \n\n Numero de Parcour = " + nbparcour + ". \n\n Prix Total = " + PRIXTOT + ". \n\n Prix + tax = "+ prixtax;
        }
    }
}
