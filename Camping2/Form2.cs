﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Camping2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private static double prixtot;

        public double PRIXTOT
        {
            get { return prixtot; }
            set { prixtot = value; }
        }

        public double Tax(double prixtax)
        {
            double prix;
            prix = prixtax + (0.15 * prixtax);
            return prix;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 frm2 = new Form2();
            frm2.Hide() ;
        }

        private void button2_Click(object sender, EventArgs e)
        {
           int nbperso = Convert.ToInt32(numericUpDown1.Value);
            int nbheur = Convert.ToInt32(numericUpDown2.Value);
            double prix=0, prixtax;

            if (radioButton1.Checked == true)
            {
                prix = 22.35;
            }
            else if (radioButton2.Checked == true)
            {
                prix = 29.55;
            }

            PRIXTOT = prix * nbperso * nbheur;
            prixtax = Tax(PRIXTOT);

            label5.Text = "Nombre de personne = " + nbperso + ". \n\n Numero d'heures = " + nbheur + ". \n\n Prix Total = " + PRIXTOT + ". \n\n Prix + tax = " + prixtax;
        }
    }
}
