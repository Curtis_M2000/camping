﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Camping2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public double Tax(double prixtax)
        {
            double prix;
            prix = prixtax + (0.15 * prixtax);
            return prix;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form3 frm3 = new Form3();
            frm3.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form2 frm2 = new Form2();
            frm2.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form4 frm4 = new Form4();
            frm4.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 frm2 = new Form2();
            Form3 frm3 = new Form3();
            Form4 frm4 = new Form4();
            double prix = 0, prixtot, prixtax, prixsej;
            DateTime date1 = dateTimePicker1.Value;
            DateTime date2 = dateTimePicker2.Value;
            DateTime i;
            double datediff = (date2 - date1).TotalDays;
            int nbperso = Convert.ToInt32(numericUpDown1.Value), f;

            /*for (date1 = date1; date1 <= date2; )
            {
                if (date1.Month == 1 || date1.Month == 2 || date1.Month == 3 || date1.Month == 4 || date1.Month == 5)
                {
                    MessageBox.Show(Convert.ToString(date1));
                    prix += 18.90;
                    date1.AddDays(1);
                }
                else if (date1.Month == 6 || date1.Month == 7 || date1.Month == 8)
                {
                    prix += 23.25;
                }
                else if (date1.Month == 9 || date1.Month == 10 || date1.Month == 11 || date1.Month == 12)
                {
                    prix += 20.25;
                }
                date1.AddDays(1);
            }*/

            if (date1.Month == 1 || date1.Month == 2 || date1.Month == 3 || date1.Month == 4 || date1.Month == 5)
            {
                prix = 18.90;
            }
            else if (date1.Month == 6 || date1.Month == 7 || date1.Month == 8)
            {
                prix = 23.25;
            }
            else if (date1.Month == 9 || date1.Month == 10 || date1.Month == 11 || date1.Month == 12)
            {
                prix = 20.25;
            }

            prixsej = prix * nbperso * datediff;
            prixtot = prixsej + frm3.PRIXTOT + frm2.PRIXTOT + frm4.PRIXTOT;
            prixtax = Tax(prixtot);

            label9.Text = "Mr./Mme " + textBox1.Text + "\n Date de debut : " + date1 + "\n Date de fin : " + date2 + "\n Nombre de personne : " + numericUpDown1.Value + "\n Prix d'equitation : " + frm3.PRIXTOT + "\n Prix de canon :" + frm2.PRIXTOT + "\n prix d'escalade : " + frm4.PRIXTOT + "\n Prix de sejour : " + prixsej + "\n prix total : " + prixtot + "\n Prix + tax : " + prixtax;
        }
    }
}
