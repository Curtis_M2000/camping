﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Camping2
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private static double prixtot;

        public double PRIXTOT
        {
            get { return prixtot; }
            set { prixtot = value; }
        }

        public double Tax(double prixtax)
        {
            double prix;
            prix = prixtax + (0.15 * prixtax);
            return prix;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form4 frm4 = new Form4();
            frm4.Hide();
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            int nbperso = Convert.ToInt32(numericUpDown3.Value);
            int nbheur = Convert.ToInt32(numericUpDown4.Value);
            double prixtax;

            PRIXTOT = 10.00 * nbheur * nbperso;
            prixtax = Tax(PRIXTOT);

            label8.Text = "Nombre de personne = " + nbperso + ". \n\n Numero d'heures = " + nbheur + ". \n\n Prix Total = " + PRIXTOT + ". \n\n Prix + tax = " + prixtax;
        }
    }
}
